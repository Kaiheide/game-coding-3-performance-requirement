using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CrowbarSwinging : MonoBehaviour
{

    private Animator animator;

    private bool isHitting;

    private CrowbarInput controlls;

    [SerializeField] private float animationFinishTime = 0.9f;


    void Awake()
    {
        controlls = new CrowbarInput();
        animator = GetComponent<Animator>();
        controlls.BarInput1.Attack.performed += context => Attack();
    }


    void Attack()
    {
        if (!isHitting)
        {
            animator.SetTrigger("isHitting");
            StartCoroutine(InitializeAttack());
        }
    }



    IEnumerator InitializeAttack()
    {
        yield return new WaitForSeconds(0.1f);
        isHitting = true;
    }


    void Update()
    {
        if (isHitting && animator.GetCurrentAnimatorStateInfo(1).normalizedTime >= animationFinishTime)
        {
            isHitting = false;
        }
    }




    private void OnEnable()
    {
        controlls.Enable();
    }

    private void OnDisable()
    {
        controlls.Disable();
    }



    void Start()
    {

    }
}