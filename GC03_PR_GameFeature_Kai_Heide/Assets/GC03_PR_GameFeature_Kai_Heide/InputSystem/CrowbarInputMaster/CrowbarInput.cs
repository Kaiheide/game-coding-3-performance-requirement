// GENERATED AUTOMATICALLY FROM 'Assets/GC03_PR_GameFeature_Kai_Heide/InputSystem/CrowbarInputMaster/CrowbarInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CrowbarInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @CrowbarInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CrowbarInput"",
    ""maps"": [
        {
            ""name"": ""BarInput1"",
            ""id"": ""8696a513-430f-44ad-aa33-0ce18be4a430"",
            ""actions"": [
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""63f20027-17cb-4743-8c9a-cb11a6e9ef86"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9746355b-0dd1-49ef-a903-62b44013b93e"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // BarInput1
        m_BarInput1 = asset.FindActionMap("BarInput1", throwIfNotFound: true);
        m_BarInput1_Attack = m_BarInput1.FindAction("Attack", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // BarInput1
    private readonly InputActionMap m_BarInput1;
    private IBarInput1Actions m_BarInput1ActionsCallbackInterface;
    private readonly InputAction m_BarInput1_Attack;
    public struct BarInput1Actions
    {
        private @CrowbarInput m_Wrapper;
        public BarInput1Actions(@CrowbarInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Attack => m_Wrapper.m_BarInput1_Attack;
        public InputActionMap Get() { return m_Wrapper.m_BarInput1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BarInput1Actions set) { return set.Get(); }
        public void SetCallbacks(IBarInput1Actions instance)
        {
            if (m_Wrapper.m_BarInput1ActionsCallbackInterface != null)
            {
                @Attack.started -= m_Wrapper.m_BarInput1ActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_BarInput1ActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_BarInput1ActionsCallbackInterface.OnAttack;
            }
            m_Wrapper.m_BarInput1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
            }
        }
    }
    public BarInput1Actions @BarInput1 => new BarInput1Actions(this);
    public interface IBarInput1Actions
    {
        void OnAttack(InputAction.CallbackContext context);
    }
}
